package com.cluby.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.cluby.R;


/**
 * This class has methods to load an image from url and show it as a ImageView
 *
 * @author W&L
 * @version 1.0
 */
public class ImageLoader {

    /**
     * MemoryCache attribute to use cache to save bitmap objects
     */
    MemoryCache memoryCache = new MemoryCache();

    /**
     * FileCache attribute to manage file in cache
     */
    FileCache fileCache;

    private int width;

    /**
     * Map attribute to put imageView object to be reused, using a
     * WeakHashMap context (http://developer.android.com/reference/java/util/WeakHashMap.html)
     */
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());

    /**
     * The java.util.concurrent.ExecutorService interface represents an asynchronous execution mechanism which
     * is capable of executing tasks in the background. An ExecutorService is thus very similar to a thread pool.
     * In fact, the implementation of ExecutorService present in the java.util.concurrent package is a thread pool
     * implementation.
     */
    private ExecutorService executorService;

    /**
     * int attribute that gets the ic_launcher icon id
     */
    private int stubId = R.drawable.ic_launcher;


    private ProgressBar progressBar;

    public ImageLoader(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
    }


    /**
     * This method sets a bitmap object into imageView parameter, it uses cache to display it
     *
     * @param url
     * @param loader
     * @param imageView
     */
    public void DisplayImage(String url, int loader, ImageView imageView, int widthScreen, ProgressBar pbar) {
        stubId = loader;
        imageViews.put(imageView, url);

        progressBar = pbar;

        width = widthScreen;

        Bitmap bitmap = memoryCache.get(url);
        if (bitmap != null)
            imageView.setImageBitmap(bitmap);
        else {
            queuePhoto(url, imageView);
            //imageView.setImageResource(loader);
            //ProgressBar progressBar =
        }
    }


    public void DisplayImage(String url, int loader, ImageView imageView, int widthScreen) {
        stubId = loader;
        imageViews.put(imageView, url);

        width = widthScreen;

        Bitmap bitmap = memoryCache.get(url);
        if (bitmap != null)
            imageView.setImageBitmap(bitmap);
        else {
            queuePhoto(url, imageView);
            imageView.setImageResource(loader);
        }
    }


    /**
     * This method uses the executorService to prepare a image to load
     *
     * @param url
     * @param imageView
     */
    private void queuePhoto(String url, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    /**
     * this method obtains a bitmap object from fileCacheobject
     *
     * @param url
     * @return Bitmap
     */
    private Bitmap getBitmap(String url) {
        File f = fileCache.getFile(url);

        //from SD cache
        Bitmap b = decodeFile(f);
        if (b != null)
            return b;

        //from web
        try {
            Bitmap bitmap = null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            bitmap = decodeFile(f);
            return bitmap;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Decodes image and scales it to reduce memory consumption
     */
    private Bitmap decodeFile(File f) {
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //Find the correct scale value. It should be the power of 2.
            //final int REQUIRED_SIZE = 70;
            final int REQUIRED_SIZE = width;



            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    /**
     * Task for the queue
     */
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }


    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            Bitmap bmp = getBitmap(photoToLoad.url);
            memoryCache.put(photoToLoad.url, bmp);
            if (imageViewReused(photoToLoad))
                return;
            BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
            Activity a = (Activity) photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {

            if (imageViewReused(photoToLoad))
                return;
            if (bitmap != null){

                if (progressBar != null) progressBar.setVisibility(View.GONE);
                photoToLoad.imageView.setImageBitmap(bitmap);
            }
            else{
                if (progressBar != null) progressBar.setVisibility(View.GONE);
                photoToLoad.imageView.setImageResource(stubId);
            }
        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

}
