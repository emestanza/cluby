package com.cluby.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

/**
 * This class is used to declare any constans and methods that the app needs
 *
 * @author W&L
 * @version 1.0
 */
public class AppConfig {

    //public static final String BASE_API_URL= "http://dev.cluby-app.com";
    public static final String BASE_API_URL= "http://192.168.0.100:8000";
    public static final String LOGIN_POST_URL = BASE_API_URL+"/oauth2/access_token";
    public static final String NEW_EVENT_URL = BASE_API_URL+"/api/v1/event/";
    public static final String GET_EVENTS_URL = BASE_API_URL+"/events";
    public static final String GET_POST_EVENT_URL = BASE_API_URL+"/posts_events";
    public static final String OAUTH_GRANT_TYPE = "password";

    //public static final String OAUTH_CLIENT_SECRET = "3b0da960d5ea61b0df1389ad76af9276bb2ee8e2";
    public static final String OAUTH_CLIENT_SECRET = "125df73d2057de944c8331054c2165d1f3eddbbe";

    //public static final String OAUTH_CLIENT_ID = "701c1a32d3f071750789";
    public static final String OAUTH_CLIENT_ID = "b468b0dbc98d45e47e0c";

    public static final String PREFS_NAME = "ClubyPrefsFile";
    public static final String CLUBY_ACCESS_TOKEN= "cluby_session_access_token";

    /**
     * This method returns a List with some parameters to be used in a http
     * request
     */
    public static List getMainParameters() {
        List parameters = new ArrayList();
        parameters.add(new BasicNameValuePair("grant_type", OAUTH_GRANT_TYPE));
        parameters.add(new BasicNameValuePair("client_id", OAUTH_CLIENT_ID));
        parameters.add(new BasicNameValuePair("client_secret", OAUTH_CLIENT_SECRET));
        parameters.add(new BasicNameValuePair("scope", OAUTH_SCOPE));

        return parameters;
    }

    public static final String OAUTH_SCOPE = "write";
}
