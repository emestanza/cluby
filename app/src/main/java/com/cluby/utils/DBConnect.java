package com.cluby.utils;

import java.util.ArrayList;

import com.cluby.model.User;

public class DBConnect {

    public ArrayList<User> users;
    private static DBConnect instance = null;

    public DBConnect() {
        users = new ArrayList<User>();
        users.add(new User("usuario@gmail.com", "12345"));
        users.add(new User("usuario", "12345"));
    }

    public static DBConnect getInstance() {
        if (instance == null) {
            instance = new DBConnect();
        }
        return instance;
    }

}
