package com.cluby.utils;

/**
 * Created by W&L on 08/10/2014.
 */
public class HttpClubyResponse {

    private int statusCode;
    private String reasonPhrase;
    private String responseBody;

    public HttpClubyResponse(int statusCode, String reasonPhrase, String responseBody){
        this.statusCode = statusCode;
        this.reasonPhrase = reasonPhrase;
        this.responseBody = responseBody;
    }


    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }

    public void setReasonPhrase(String reasonPhrase) {
        this.reasonPhrase = reasonPhrase;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }
}
