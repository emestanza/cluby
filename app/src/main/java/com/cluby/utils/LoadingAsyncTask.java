package com.cluby.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import com.cluby.R;


/**
 * Class used to set a loading dialog while information or activities are loaded
 * Created by W&L on 14/05/2014.
 */
public class LoadingAsyncTask extends AsyncTask<Void, Void, Void> {

    ProgressDialog dialog;

    Context context;
    Intent intentObj;


    public LoadingAsyncTask(Context c, Intent intent) {
        context = c;
        intentObj = intent;
    }


    @Override
    protected void onPreExecute() {
        ProgressDialog dialog = new ProgressDialog(context);
        //set message of the dialog
        dialog.setMessage(context.getResources().getString(R.string.loadingBgStr));
        //show dialog
        dialog.show();
        super.onPreExecute();
    }

    protected Void doInBackground(Void... args) {
        // do background work here

        context.startActivity(intentObj);
        return null;
    }

    protected void onPostExecute(Void result) {
        // do UI work here
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }

    }
}