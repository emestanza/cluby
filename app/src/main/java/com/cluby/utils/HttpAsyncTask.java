package com.cluby.utils;

import java.io.IOException;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import android.content.Context;
import android.os.AsyncTask;
import org.apache.http.util.EntityUtils;

/**
 * This class is made to provide http requests (get, post)
 *
 * @author W&L
 * @version 1.0
 */
public class HttpAsyncTask extends AsyncTask<List, Void, HttpClubyResponse> {
    Context context;
    String method;
    String appType;
    String json;
    String token;
    String responseBody = "<Empty response body>";
    HttpResponse httpResponse = null;

    public HttpAsyncTask(Context c, String methodToExec) {
        context = c;
        method = methodToExec;
        appType= "";
    }

    public HttpAsyncTask(Context c, String methodToExec, String appTypeExec) {
        context = c;
        method = methodToExec;
        appType= appTypeExec;
    }

    public HttpAsyncTask(Context c, String methodToExec, String appTypeExec, String jsonStr, String accessToken) {
        context = c;
        method = methodToExec;
        appType= appTypeExec;
        json = jsonStr;
        token = accessToken;
    }

    /**
     * This function is going to be executed when the instance invokes the
     * execute() method
     *
     * @param urls
     */
    @Override
    protected HttpClubyResponse doInBackground(List... urls) {
        return getResult(urls);
    }

    /**
     * After doInBackground() is executed, this method will be invoked
     *
     * @param result
     */
    @Override
    protected void onPostExecute(HttpClubyResponse result) {
        super.onPostExecute(result);
    }

    /**
     * This method will process the request, returning a JSON string
     *
     * @param urls
     */
    public HttpClubyResponse getResult(List[] urls) {

        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            List aux = urls[0];
            BasicNameValuePair url = (BasicNameValuePair) aux
                    .get(aux.size() - 1);
            // make GET request to the given URL

            aux.remove(aux.size() - 1);

            if (method.equals("get")) {
                HttpGet reqObj = new HttpGet(url.getValue().toString());
                httpResponse = httpclient.execute(reqObj);
            }

            if (method.equals("post")) {
                // specify the URL you want to post to
                HttpPost reqObj = new HttpPost(url.getValue().toString());

                if (appType.equals("json")) {

                    reqObj.setHeader("Content-type", "application/json");
                    reqObj.setHeader("Authorization", token);
                    reqObj.setEntity(new ByteArrayEntity(json.toString().getBytes("UTF8")));
                }
                else if (appType.equals("multipart/form-data")){

                    reqObj.setHeader("Authorization", token);
                    reqObj.setEntity((HttpEntity)aux.get(0));
                }
                else{
                    reqObj.setEntity(new UrlEncodedFormEntity(aux));
                }

                httpResponse = httpclient.execute(reqObj);
            }

            HttpEntity entityResponse = httpResponse.getEntity();
            responseBody = EntityUtils.toString(entityResponse);

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (IllegalArgumentException e){
            responseBody = "<Empty response body>";
        }

        return new HttpClubyResponse(httpResponse.getStatusLine().getStatusCode(), httpResponse.getStatusLine().getReasonPhrase(), responseBody);
    }

}