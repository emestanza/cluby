package com.cluby.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.widget.Toast;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * This class contains several common methods to be used in all the app
 *
 * @author W&L
 * @version 1.0
 */
public class Utils {

    /*
    * This method displays a Toast message in the app
    */
    public static void sendUIMeesage(Context context, CharSequence text) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(Gravity.TOP | Gravity.TOP, 0, 25);
        toast.show();
    }

    /*
    * This method displays an Alert message in the app
    *
    * @param context
    * @param messageHeader
    * @param messageContent
    */
    public static void sendAlertMessage(Context context, String messageHeader, String messageContent){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(messageContent).setTitle(messageHeader);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*
    * This method makes a copy of one InputStream object
    */
    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }


    /**
     * Checks if there is a internet connection active
     *
     * @param context application context
     * @return boolean
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }

    public static String dateToISO(String date, String time) throws java.text.ParseException{

        String[] auxDate =date.split("/");
        String string = auxDate[1]+" "+auxDate[0]+" "+auxDate[2]+" "+time;
        Date dateAux = new SimpleDateFormat("MM dd yyyy hh:mm a", Locale.ENGLISH).parse(string);

        TimeZone tz = TimeZone.getDefault();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //1989-02-01T06:40:30
        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(tz);

        return df.format(dateAux);
    }

}
