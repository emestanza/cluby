package com.cluby;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.widget.DatePicker;
import android.widget.EditText;
import java.util.Calendar;

/**
 * This class is used to create a datepicker dialog
 * Created by W&L on 07/05/2014.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private int dateIdTxt;


    public DatePickerFragment(int dateId){
        dateIdTxt = dateId;
    }

    /**
     * Override to build your own custom Dialog container. This is typically used to show an AlertDialog instead
     * of a generic Dialog; when doing so, onCreateView(LayoutInflater, ViewGroup, Bundle) does not need to be
     * implemented since the AlertDialog takes care of its own content.
     *
     * @param savedInstanceState
     * @return DatePickerDialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    /**
     * process the result after dialog is closed
     *
     * @param view
     * @param year
     * @param month
     * @param day
     */
    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        FragmentActivity aux = getActivity();
        EditText txt = (EditText)aux.findViewById(dateIdTxt);
        txt.setText(day+"/"+(month+1)+"/"+year);

    }
}