package com.cluby;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.ViewGroup;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import com.cluby.model.EventUI;
import com.cluby.utils.FontsOverride;

/**
 * Created by W&L on 01/06/2014.
 */
@SuppressLint("NewApi")
public class EventDetailPager extends ActionBarActivity implements ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
     * three primary sections of the app. We use a {@link android.support.v4.app.FragmentPagerAdapter}
     * derivative, which will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private AppSectionsPagerAdapter appSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will display the three primary sections of the app, one at a
     * time.
     */
    private ViewPager viewPager;

    /**
     * Bean used to get the event info
     */
    private EventUI eventObj;

    /*
     * ShareActionProvider object used to provide share option in  the action bar
     */
    private ShareActionProvider shareActionProvider;

    /**
     * ActionBar attribute to manage the top action bar items
     */
    private ActionBar actionBar;

    /**
     * This method overrides the onCreate method of Activity class Initialize
     * activity components
     *
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_detail_pager);
        initElements();

    }


    /**
     * Called when a tab exits the selected state.
     *
     * @param tab
     * @param fragmentTransaction
     */
    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * Called when a tab enters the selected state.
     *
     * @param tab
     * @param fragmentTransaction
     */
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in the ViewPager.
        viewPager.setCurrentItem(tab.getPosition());
    }

    /**
     * Called when a tab that is already selected is chosen again by the user. Some applications may use this action to
     * return to the top level of a category.
     *
     * @param tab
     * @param fragmentTransaction
     */
    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        private EventUI eventObjFr;

        private Fragment fragment;

        private Context contextObj;

        private Bundle args;

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setContext(Context context){
            contextObj = context;
        }

        public void setEventObjFr(EventUI eventObj){
            eventObjFr = eventObj;
        }

        @Override
        public Fragment getItem(int i) {

                  switch (i){
                      case 0:
                          fragment = new EventDetailFragment();
                          args = new Bundle();
                          args.putSerializable("eventObjFr", eventObjFr);
                          fragment.setArguments(args);
                      break;

                      case 1:
                          fragment = new HappenNowFragment();
                      break;

                  }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String tabTitle ="";

            if (position == 0){
                tabTitle = contextObj.getResources().getString(R.string.informationStr);
            }

            if (position == 1){
                tabTitle = contextObj.getResources().getString(R.string.happenNowStr);
            }

            return tabTitle;
        }
    }

    /**
     * This method listen to onClick events and execute some sentences depending
     * on the clicked component
     *
     * @param v View object
     */
    public void ButtonOnClick(View v) {
        Intent intent = null;
        switch (v.getId()) {

            case R.id.evtDetailMap:
                intent = new Intent(EventDetailPager.this, EventMapPanoramaFragment.class);
                intent.putExtra("latitude", eventObj.getEventLatitude());
                intent.putExtra("longitude", eventObj.getEventLongitude());
                startActivity(intent);
                break;

            case R.id.evtDetailStreetView:
                intent = new Intent(EventDetailPager.this, EventMapPanoramaFragment.class);
                intent.putExtra("latitude", eventObj.getEventLatitude());
                intent.putExtra("longitude", eventObj.getEventLongitude());

                startActivity(intent);

                break;

            case R.id.evtDetailAddress:
                intent = new Intent(EventDetailPager.this, EventRouteActivity.class);
                intent.putExtra("latitude", eventObj.getEventLatitude());
                intent.putExtra("longitude", eventObj.getEventLongitude());

                startActivity(intent);
                break;

            case R.id.evtDetailRoute:
                intent = new Intent(EventDetailPager.this, EventRouteActivity.class);
                intent.putExtra("latitude", eventObj.getEventLatitude());
                intent.putExtra("longitude", eventObj.getEventLongitude());

                startActivity(intent);

                break;

        }
    }

    /**
     * Defines a default (dummy) share intent to initialize the action provider.
     * However, as soon as the actual content to be used in the intent
     * is known or changes, you must update the share intent by again calling
     * mShareActionProvider.setShareIntent()
     */
    private Intent getDefaultIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        return intent;
    }

    /**
     * This method overrides the onCreateOptionsMenu method of Activity class
     * Initialize menu components
     *
     * @param menu Menu class instance
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.evt_detail_menu, menu);

        // Set up ShareActionProvider's default share intent
        MenuItem shareItem = menu.findItem(R.id.action_share);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        shareActionProvider.setShareIntent(getDefaultIntent());

        return super.onCreateOptionsMenu(menu);

    }

    /**
     * This method links the activity components with the class components
     * (EditText, Button , etc)
     */
    public void initElements() {

        eventObj = (EventUI) getIntent().getSerializableExtra("eventObj");

        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        appSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());
        appSectionsPagerAdapter.setEventObjFr(eventObj);
        appSectionsPagerAdapter.setContext(getApplicationContext());

        // Set up the action bar.
        actionBar = getActionBar();

        // Specify that the Home/Up button should not be enabled, since there is no hierarchical
        // parent.
        actionBar.setHomeButtonEnabled(false);

        // Specify that we will be displaying tabs in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(appSectionsPagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
                actionBar.setSelectedNavigationItem(position);
            }
        });

        String tabTitle = "";
        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < appSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.

            actionBar.addTab(
                    actionBar.newTab()
                            .setText(appSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }


        // setting up the action bar
        actionBar.setTitle(getResources().getString(R.string.backStr));
        actionBar.setDisplayHomeAsUpEnabled(true);

        final Typeface mFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf");
        final ViewGroup mContainer = (ViewGroup) findViewById(android.R.id.content).getRootView();
        FontsOverride.setAppFont(mContainer, mFont, true);

    }

}