package com.cluby;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.cluby.utils.FontsOverride;
import com.cluby.utils.ImageLoader;
import com.cluby.utils.BitmapWorkerTask;
import com.cluby.utils.Utils;
import com.cluby.utils.AppConfig;
import com.cluby.utils.HttpClubyResponse;
import com.cluby.utils.HttpAsyncTask;
import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import com.facebook.UiLifecycleHelper;

/**
 * Created by W&L on 07/05/2014.
 */
public class NewEventActivity extends ActionBarActivity implements ValidationListener {

    /**
     * ActionBar attribute to manage the top action bar items
     */
    private ActionBar actionBar;

    /**
     * EditText attribute to manage the name of the new event
     */
    @Required(order = 1, message = "El nombre del evento es requerido")
    private EditText eventNameTxt;

    /**
     * EditText attribute to manage date picker to set date info for new event
     */
    @Required(order = 2, message = "Por favor, dinos la fecha de tu evento")
    private EditText eventDateTxt;

    /**
     * EditText attribute to manage date picker to set end date info for new event
     */
    private EditText eventDateEndTxt;

    /**
     * EditText attribute to manage time picker to set time info for new event
     */
    @Required(order = 3, message = "Por favor, dinos la hora de tu evento")
    private EditText eventTimeTxt;

    /**
     * EditText attribute to manage time picker to set end time info for new event
     */
    private EditText eventTimeEndTxt;

    /**
     * EditText attribute to get the description of the event
     */
    @Required(order = 4, message = "Por favor, describe tu evento")
    private EditText eventDescTxt;

    /**
     * EditText attribute to get the description of the event
     */
    @Required(order = 5, message = "Por favor, indica una dirección para tu evento")
    private EditText eventAddressTxt;

    /**
     * Spinner attribute to show the event types available
     */
    private Spinner eventTypeSpinner;

    /**
     * ImageView attribute to manage image browser to set an image to new event
     */
    private ImageView eventImgView;

    /**
     * Validator attribute to set all the validations to the login form
     */
    private Validator validator;

    /**
     * Bitmap attribute to be used in image loading async task
     */
    private Bitmap mPlaceHolderBitmap;

    /**
     * A request code's purpose is to match the result of a "startActivityForResult" with
     * the type of the original request.  Choose any value.
     */
    private static final int READ_REQUEST_CODE = 1337;

    /**
     * A request code's purpose is to match the result of a "startActivityForResult" with
     * the type of the original request.  Choose any value.
     */
    private static final int GET_MAPA_DATA_REQUEST = 1338;

    /**
     * LinearLayout to show the guest list (facebook button, and email list)
     */
    private LinearLayout invitePeopleLayout;

    /**
     * This attribute will be used to get the latitude from the current event
     */
    private Double eventMapLatitude;

    /**
     * This attribute will be used to get the longitude from the current event
     */
    private Double eventMapLongitude;

    /**
     * LinearLayout to be used to add several email text fields
     */
    private LinearLayout emailList;

    /**
     * This List will be used to ask for permissions when user use the facebook friend list
     */
    private static final List<String> PERMISSIONS = new ArrayList<String>() {
        {
            add("user_friends");
            add("public_profile");
        }
    };

    /**
     * A request code's purpose is to match the result of a "startActivityForResult" with
     * the type of the original request.  Choose any value.
     */
    private static final int PICK_FRIENDS_ACTIVITY = 1339;

    /**
     * Button to open the facebook friend list
     */
    private Button pickFriendsButton;

    /**
     * TextView to show the facebook friends selected
     */
    private TextView resultsTextView;

    /**
     * This class helps to create, automatically open (if applicable), save, and restore the Active Session in a way that
     * is similar to Android UI lifecycles.
     */
    private UiLifecycleHelper lifecycleHelper;

    /**
     * Boolean to know when session is opened correctly to start pick friend activity
     */
    boolean pickFriendsWhenSessionOpened;

    /**
     * ImageView to set the static map result of the map pin of the new event
     */
    private ImageView eventMapResult;


    public Uri uri = null;

    /**
     * This method overrides the onCreate method of Activity class Initialize
     * activity components
     *
     * @param savedInstanceState
     */
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.new_event_activity);
        initElements(savedInstanceState);

    }

    @Override
    protected void onStart() {
        super.onStart();

        // Update the display every time we are started.
        displaySelectedFriends(RESULT_OK);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising reporting.  Do so in
        // the onResume methods of the primary Activities that an app may be launched into.
        AppEventsLogger.activateApp(this);
    }


    /**
     * This method links the activity components with the class components
     * (EditText, Button , etc)
     */
    @SuppressLint("NewApi")
    public void initElements(Bundle savedInstanceState) {

        // setting up the action bar
        actionBar = getActionBar();
        actionBar.setTitle(getResources().getString(R.string.newEventStr));
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        Spinner spinner = (Spinner) findViewById(R.id.eventTypeSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.eventTypeArray, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        eventNameTxt = (EditText) findViewById(R.id.eventNameTxt);
        eventDescTxt = (EditText) findViewById(R.id.eventDescTxt);
        eventAddressTxt = (EditText) findViewById(R.id.eventAddressTxt);

        eventDateTxt = (EditText) findViewById(R.id.dateTxt);
        eventDateEndTxt = (EditText) findViewById(R.id.dateEndTxt);
        eventTimeTxt = (EditText) findViewById(R.id.timeTxt);
        eventTimeEndTxt = (EditText) findViewById(R.id.timeEndTxt);
        eventImgView = (ImageView) findViewById(R.id.getImgBtn);
        eventTypeSpinner = (Spinner) findViewById(R.id.eventTypeSpinner);
        invitePeopleLayout = (LinearLayout) findViewById(R.id.invitePeopleLayout);
        validator = new Validator(this);
        validator.setValidationListener(this);
        emailList = (LinearLayout) findViewById(R.id.eventEmailList);
        eventMapResult = (ImageView) findViewById(R.id.eventMapResult);
        resultsTextView = (TextView) findViewById(R.id.resultsTextView);
        pickFriendsButton = (Button) findViewById(R.id.pickFriendsButton);
        pickFriendsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onClickPickFriends();
            }
        });

        lifecycleHelper = new UiLifecycleHelper(this, new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                onSessionStateChanged(session, state, exception);
            }
        });
        lifecycleHelper.onCreate(savedInstanceState);
        //ensureOpenSession();

        eventDateTxt.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    showDatePickerDialog(view);
                }

                return false;
            }
        });

        eventDateEndTxt.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    showDatePickerDialog(view);
                }

                return false;
            }
        });

        eventTimeTxt.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    showTimePickerDialog(view);
                }

                return false;
            }
        });

        eventTimeEndTxt.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    showTimePickerDialog(view);
                }

                return false;
            }
        });

        final Typeface mFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf");
        final ViewGroup mContainer = (ViewGroup) findViewById(android.R.id.content).getRootView();
        FontsOverride.setAppFont(mContainer, mFont, true);
    }

    /**
     * This method shows a datepicker dialog in UI
     *
     * @param v View object
     */
    public void showDatePickerDialog(View v) {

        DialogFragment newFragment = new DatePickerFragment(v.getId());
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    /**
     * This method shows a timepicker dialog in UI
     *
     * @param v View object
     */
    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment(v.getId());
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    /**
     * This method listen to onClick events and execute some sentences depending
     * on the clicked component
     *
     * @param v View object
     */
    public void ButtonOnClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.getMapBtn:
                Log.d("com.cluby.NewEventTest.getMap", "get map instance");
                eventMapResult.setImageResource(android.R.color.transparent);
                intent = new Intent(NewEventActivity.this, GetMapActivity.class);
                startActivityForResult(intent, GET_MAPA_DATA_REQUEST);
                break;

            case R.id.getImgBtn:
                performFileSearch();
                break;

            case R.id.newEmailBtn:

                LayoutInflater vi = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                emailList.addView(vi.inflate(R.layout.event_email_layout, null));

                break;
            case R.id.invitePeoplEventBtn:
                invitePeopleLayout.setVisibility(View.VISIBLE);
                break;

            case R.id.createEventBtn:
                validator.validate();
                break;
        }
    }

    /**
     * Fires an intent to spin up the "file chooser" UI and select an image.
     */
    public void performFileSearch() {

        // BEGIN_INCLUDE (use_open_document_intent)
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a file (as opposed to a list
        // of contacts or timezones)
        intent.setAction(Intent.ACTION_GET_CONTENT);

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers, it would be
        // "*/*".
        intent.setType("image/*");

        startActivityForResult(intent, READ_REQUEST_CODE);
        // END_INCLUDE (use_open_document_intent)
    }


    /**
     * Called when an activity you launched exits, giving you the requestCode you started it with, the resultCode
     * it returned, and any additional data from it. The resultCode will be RESULT_CANCELED if the activity
     * explicitly returned that, didn't return any result, or crashed during its operation.
     *
     * @param requestCode
     * @param resultCode
     * @param resultData
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        //Log.i(TAG, "Received an \"Activity Result\"");
        // BEGIN_INCLUDE (parse_open_document_response)
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code READ_REQUEST_CODE.
        // If the request code seen here doesn't match, it's the response to some other intent,
        // and the below code shouldn't run at all.

        switch (requestCode) {
            case PICK_FRIENDS_ACTIVITY:
                displaySelectedFriends(resultCode);
                break;
            case GET_MAPA_DATA_REQUEST:

                if (resultData != null) {

                    eventMapLatitude = resultData.getDoubleExtra("mapLatitude", 0);
                    eventMapLongitude = resultData.getDoubleExtra("mapLongitude", 0);

                    ImageLoader imgLoader = new ImageLoader(this);
                    DisplayMetrics displaymetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

                    int height = displaymetrics.heightPixels;
                    int width = displaymetrics.widthPixels;

                    ProgressBar pbar = (ProgressBar) findViewById(R.id.loadingPanel);
                    pbar.setVisibility(View.VISIBLE);
                    imgLoader.DisplayImage(getStaticMapUrl(eventMapLatitude, eventMapLongitude), R.drawable.loader, eventMapResult, width, pbar);

                }

                break;
            case READ_REQUEST_CODE:
                // The document selected by the user won't be returned in the intent.
                // Instead, a URI to that document will be contained in the return intent
                // provided to this method as a parameter.  Pull that uri using "resultData.getData()"

                if (resultData != null) {
                    uri = resultData.getData();
                    loadBitmap(uri, eventImgView);
                }
                // END_INCLUDE (parse_open_document_response)

                break;
            default:
                Session.getActiveSession().onActivityResult(this, requestCode, resultCode, resultData);
                break;
        }

    }


    /**
     * Returns the api url to get a static map from api static map
     *
     * @param latitude
     * @param longitude
     * @return
     */
    private String getStaticMapUrl(Double latitude, Double longitude) {

        // Building the url to the web service
        String url = "http://maps.googleapis.com/maps/api/staticmap?scale=2&markers=color:red%7Ccolor:red%7C" + latitude + "," + longitude + "&size=600x600&zoom=17&key=" + getResources().getString(R.string.googleApiKey);
        return url;
    }

    /**
     * Returns boolean in case of session is active and opened
     *
     * @return boolean
     */
    private boolean ensureOpenSession() {
        if (Session.getActiveSession() == null ||
                !Session.getActiveSession().isOpened()) {
            Session.openActiveSession(
                    this,
                    true,
                    PERMISSIONS,
                    new Session.StatusCallback() {
                        @Override
                        public void call(Session session, SessionState state, Exception exception) {
                            onSessionStateChanged(session, state, exception);
                        }
                    });
            return false;
        }
        return true;
    }

    /**
     * Return true-false in case of having the correct permission when app use facebook app
     *
     * @param session
     * @return boolean
     */
    private boolean sessionHasNecessaryPerms(Session session) {
        if (session != null && session.getPermissions() != null) {
            for (String requestedPerm : PERMISSIONS) {
                if (!session.getPermissions().contains(requestedPerm)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }


    /**
     * Returns a List of missing permissions in case of find at least one
     *
     * @param session
     * @return List
     */
    private List<String> getMissingPermissions(Session session) {
        List<String> missingPerms = new ArrayList<String>(PERMISSIONS);
        if (session != null && session.getPermissions() != null) {
            for (String requestedPerm : PERMISSIONS) {
                if (session.getPermissions().contains(requestedPerm)) {
                    missingPerms.remove(requestedPerm);
                }
            }
        }
        return missingPerms;
    }


    /**
     * Method used by facebook sdk for session topics
     *
     * @param session
     * @param state
     * @param exception
     */
    private void onSessionStateChanged(final Session session, SessionState state, Exception exception) {

        if (state.isOpened() && !sessionHasNecessaryPerms(session)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.need_perms_alert_text);
            builder.setPositiveButton(
                    R.string.need_perms_alert_button_ok,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            session.requestNewReadPermissions(
                                    new Session.NewPermissionsRequest(
                                            NewEventActivity.this,
                                            getMissingPermissions(session)));
                        }
                    });
            builder.setNegativeButton(
                    R.string.need_perms_alert_button_quit,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            builder.show();
        } else if (pickFriendsWhenSessionOpened && state.isOpened()) {
            pickFriendsWhenSessionOpened = false;

            startPickFriendsActivity();
        }
    }


    /**
     * Prints facebook friends selected into UI
     *
     * @param resultCode
     */
    private void displaySelectedFriends(int resultCode) {
        String results = "";
        FriendPickerApplication application = (FriendPickerApplication) getApplication();

        Collection<GraphUser> selection = application.getSelectedUsers();
        if (selection != null && selection.size() > 0) {
            ArrayList<String> names = new ArrayList<String>();
            for (GraphUser user : selection) {
                names.add(user.getName());
            }
            results = TextUtils.join(", ", names);
        } else {
            results = "<No friends selected>";
        }

        resultsTextView.setText(results);
    }


    /**
     * Method to trigger the facebook picker friends activity
     */
    private void onClickPickFriends() {
        startPickFriendsActivity();
    }

    /**
     * Displays the facebook friend list
     */
    private void startPickFriendsActivity() {
        if (ensureOpenSession()) {
            Intent intent = new Intent(NewEventActivity.this, PickFriendsActivity.class);
            // Note: The following line is optional, as multi-select behavior is the default for
            // FriendPickerFragment. It is here to demonstrate how parameters could be passed to the
            // friend picker if single-select functionality was desired, or if a different user ID was
            // desired (for instance, to see friends of a friend).
            PickFriendsActivity.populateParameters(intent, null, true, true);
            startActivityForResult(intent, PICK_FRIENDS_ACTIVITY);
        } else {
            pickFriendsWhenSessionOpened = true;
        }
    }

    /**
     * Method called to load a bitmap from uri, all of this invoking a BitmapWorkerTask object
     *
     * @param uri       Uri object to be loaded
     * @param imageView ImageView object to be used to set the result
     */
    public void loadBitmap(Uri uri, ImageView imageView) {

        if (BitmapWorkerTask.cancelPotentialWork(imageView)) {
            final BitmapWorkerTask task = new BitmapWorkerTask(getApplicationContext(), imageView);
            final BitmapWorkerTask.AsyncDrawable asyncDrawable =
                    new BitmapWorkerTask.AsyncDrawable(getApplicationContext().getResources(), mPlaceHolderBitmap, task);
            imageView.setImageDrawable(asyncDrawable);
            task.execute(uri);
        }
    }

    /**
     * This method is an override method from the preValidation method in
     * ValidationListener interface Called before the Validator begins
     * validation.
     */
    @Override
    public void preValidation() {
        // TODO Auto-generated method stub

        eventNameTxt.setError(null);
        eventDateTxt.setError(null);
        eventDateEndTxt.setError(null);
        eventTimeTxt.setError(null);
        eventTimeEndTxt.setError(null);
        eventDescTxt.setError(null);
        eventAddressTxt.setError(null);
    }

    /**
     * This method is an override method from the onSuccess method in
     * ValidationListener interface Called when all the Rules added to this
     * Validator are valid.
     */
    @Override
    public void onSuccess() {

        JSONObject jsonEvent = new JSONObject();
        try {

            String beginningDateStr = Utils.dateToISO(eventDateTxt.getText().toString(), eventTimeTxt.getText().toString());
            String endDateStr = Utils.dateToISO(eventDateEndTxt.getText().toString(), eventTimeEndTxt.getText().toString());

            jsonEvent.put("type", "/api/v1/events/type/1/");
            jsonEvent.put("ending", endDateStr);
            jsonEvent.put("beginning", beginningDateStr);
            jsonEvent.put("description", eventDescTxt.getText());
            jsonEvent.put("name", eventNameTxt.getText());

            EditText emailAuxTxt;
            ArrayList<String> emailArr = new ArrayList<String>();
            if (emailList.getChildCount() > 0) {
                for (int i = 0; i < emailList.getChildCount(); i++) {
                    emailAuxTxt = (EditText) emailList.getChildAt(i);
                    if (!emailAuxTxt.getText().toString().equals(""))
                        emailArr.add(emailAuxTxt.getText().toString());
                }
            }

            String emailJoined = TextUtils.join(",", emailArr);
            // jsonEvent.put("eventGuests", emailJoined);

            // Restore preferences
            SharedPreferences settings = getSharedPreferences(AppConfig.PREFS_NAME, 0);
            String accessToken = settings.getString(AppConfig.CLUBY_ACCESS_TOKEN, null);

            // Call Async<Task to perform network operation on separate thread
            HttpAsyncTask reqClient = new HttpAsyncTask(this, "post", "json", jsonEvent.toString(), "Oauth " + accessToken);

            try {
                List parameters = new ArrayList();
                parameters.add(new BasicNameValuePair("url", AppConfig.NEW_EVENT_URL));

                HttpClubyResponse result = reqClient.execute(parameters).get();
                JSONObject obj = new JSONObject(result.getResponseBody());

                if (obj.has("error")) {
                    Utils.sendUIMeesage(this, getResources().getString(R.string.eventCreatedErrorStr));
                } else {

                    String eventId = obj.getString("id");
                    if (eventId != null) {

                        HttpAsyncTask requestUploadImg = new HttpAsyncTask(this, "post", "multipart/form-data", "", "Oauth " + accessToken);

                        Bitmap icon = ((BitmapDrawable) eventImgView.getDrawable()).getBitmap();
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        icon.compress(Bitmap.CompressFormat.JPEG, 100, bos);

                        parameters = new ArrayList();

                        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                        builder.addBinaryBody("image", bos.toByteArray(), ContentType.create("image/jpeg"), "evento"+eventId+".jpg");
                        HttpEntity entity = builder.build();

                        parameters.add(entity);
                        parameters.add(new BasicNameValuePair("url", AppConfig.NEW_EVENT_URL + eventId + "/image/"));

                        result = requestUploadImg.execute(parameters).get();

                        if (result.getStatusCode() == 200){
                            Intent intent = new Intent(NewEventActivity.this, WelcomeActivity.class);
                            startActivity(intent);
                            Utils.sendUIMeesage(this, getResources().getString(R.string.eventCreatedSuccessStr));
                        }

                    }

                }

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ExecutionException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    /**
     * This method is a override method from the onSuccess method in Called if
     * any of the Rules fail. Called if any of the Rules fail.
     *
     * @param failedView
     * @param failedRule
     */
    @Override
    public void onFailure(View failedView, Rule<?> failedRule) {
        // TODO Auto-generated method stub
        String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method is a override method from the onValidationCancelled method
     */
    @Override
    public void onValidationCancelled() {
        // TODO Auto-generated method stub

    }

    /**
     * This method overrides the onCreateOptionsMenu method of Activity class
     * Initialize menu components
     *
     * @param menu Menu class instance
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.new_event_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /*
    * Method called when item menu is selected
    */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        //Intent intent = null;
        switch (item.getItemId()) {
            case R.id.createEventBtn:

                validator.validate();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}