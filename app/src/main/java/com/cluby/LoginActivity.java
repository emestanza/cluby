package com.cluby;

import java.util.List;
import java.util.concurrent.ExecutionException;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup;
import android.view.View;
import com.cluby.utils.FontsOverride;
import com.cluby.utils.HttpAsyncTask;
import com.cluby.utils.Utils;
import com.cluby.utils.AppConfig;
import com.cluby.utils.HttpClubyResponse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This class is attached to the login activity, so it covers all the logic
 * related to users login
 *
 * @author W&L
 * @version 1.0
 */
public class LoginActivity extends FragmentActivity implements ValidationListener {

    /**
     * EditText attribute to init the email text field in the logic activity
     */
    @Required(order = 1, message = "Email es requerido")
    public EditText emailTxt;

    /**
     * EditText attribute to init the email text field in the logic activity
     */
    @Required(order = 2, message = "Password es requerido")
    public EditText passwordTxt;

    /**
     * Button attribute to init the login Button in the logic activity
     */
    public Button loginBtn;

    /**
     * EditText attribute to init the password text field in the logic activity
     */
    public TextView hereTxt;

    /**
     * Validator attribute to set all the validations to the login form
     */
    private Validator validator;

    /**
     * FacebookLoginFragment attribute to init facebook login button and add it into the login activity
     */
    private FacebookLoginFragment facebookLoginFragment;

    /**
     * This method overrides the onCreate method of Activity class Initialize
     * activity components
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initElements(savedInstanceState);

    }

    /**
     * This method overrides the onCreateOptionsMenu method of Activity class
     * Initialize menu components
     *
     * @param menu Menu class instance
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    /**
     * This method links the activity components with the class components
     * (EditText, Button , etc)
     */
    public void initElements(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);

        validator = new Validator(this);
        validator.setValidationListener(this);
        emailTxt = (EditText) findViewById(R.id.emailTxtLogin);
        passwordTxt = (EditText) findViewById(R.id.passwTxtLogin);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        hereTxt = (TextView) findViewById(R.id.hereTxt);

        final Typeface mFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf");
        final ViewGroup mContainer = (ViewGroup) findViewById(android.R.id.content).getRootView();
        FontsOverride.setAppFont(mContainer, mFont, true);

        /*if (savedInstanceState == null) {
            // Add the fragment on initial activity setup
            facebookLoginFragment = new FacebookLoginFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.loginFbFrame, facebookLoginFragment)
                    .commit();
        } else {
            // Or set the fragment from restored state info
            facebookLoginFragment = (FacebookLoginFragment) getSupportFragmentManager()
                    .findFragmentById(android.R.id.content);
        }*/

    }

    /**
     * This method listen to onClick events and execute some sentences depending
     * on the clicked component
     */
    public void ButtonOnClick(View v) {

        switch (v.getId()) {
            case R.id.loginBtn:
                validator.validate();
                break;

        }
    }

    /**
     * This method is an override method from the preValidation method in
     * ValidationListener interface Called before the Validator begins
     * validation.
     */
    @Override
    public void preValidation() {
        // TODO Auto-generated method stub

    }

    /**
     * This method is an override method from the onSuccess method in
     * ValidationListener interface Called when all the Rules added to this
     * Validator are valid.
     */
    @Override
    public void onSuccess() {
        // TODO Auto-generated method stub

        if (Utils.isNetworkConnected(this.getApplicationContext())) {

            // Call AsynTask to perform network operation on separate thread
            HttpAsyncTask reqClient = new HttpAsyncTask(this, "post");

            try {
                List parameters = AppConfig.getMainParameters();
                parameters.add(new BasicNameValuePair("username", emailTxt.getText().toString()));
                parameters.add(new BasicNameValuePair("password", passwordTxt.getText().toString()));
                parameters.add(new BasicNameValuePair("url", AppConfig.LOGIN_POST_URL));

                HttpClubyResponse result = reqClient.execute(parameters).get();

                if (!result.equals("")) {

                    JSONObject obj = new JSONObject(result.getResponseBody());

                    if (obj.has("error")) {
                        Log.d("com.cluby.LoginActivity.loginError", "loginError");
                        Utils.sendUIMeesage(this, getResources().getString(R.string.loginErrorStr));

                    } else {
                        Intent intent = new Intent(LoginActivity.this, LocationActivity.class);

                        //do something with access token
                        String accessToken = obj.getString("access_token");

                        // We need an Editor object to make preference changes.
                        // All objects are from android.context.Context
                        SharedPreferences settings = getSharedPreferences(AppConfig.PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(AppConfig.CLUBY_ACCESS_TOKEN, accessToken);

                        // Commit the edits!
                        editor.commit();

                        Log.d("com.cluby.LoginActivity.loginSuccess", "loginSuccess");
                        startActivity(intent);
                    }
                } else Utils.sendUIMeesage(this, getResources().getString(R.string.loginErrorStr));

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("com.cluby.LoginActivity.systemError", e.getMessage());
                Utils.sendUIMeesage(this, getResources().getString(R.string.internalErrorStr));
            } catch (ExecutionException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("com.cluby.LoginActivity.systemError", e.getMessage());
                Utils.sendUIMeesage(this, getResources().getString(R.string.internalErrorStr));
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("com.cluby.LoginActivity.systemError", e.getMessage());
                Utils.sendUIMeesage(this, getResources().getString(R.string.internalErrorStr));
            }
        } else {
            Utils.sendUIMeesage(this, getResources().getString(R.string.noInternetStr));
        }

    }

    /**
     * This method is a override method from the onSuccess method in Called if
     * any of the Rules fail. Called if any of the Rules fail.
     *
     * @param failedView
     * @param failedRule
     */
    @Override
    public void onFailure(View failedView, Rule<?> failedRule) {

        Log.d("com.cluby.LoginActivity.invalidForm", "Invalid Form");
        // TODO Auto-generated method stub
        String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method is a override method from the onValidationCancelled method
     */
    @Override
    public void onValidationCancelled() {
        // TODO Auto-generated method stub

    }


    /*
     * Check that Google Play services is available
     */
    private boolean servicesConnected() {

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {

        }
        return true;
    }

}