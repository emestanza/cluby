package com.cluby;

import android.annotation.SuppressLint;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.cluby.model.EventUI;
import com.cluby.utils.ImageLoader;

/**
 * This class is the first activity class for the app, which shows the
 * presentation of the app
 *
 * @author Enmanuel Mestanza
 * @version 1.0
 */
public class EventDetailActivity extends ActionBarActivity {

    /*
     * EventUI object to be used to process the JSON array taken by API
     */
    private EventUI eventObj;

    /*
     * TextView object used to set the event description in the activity
     */
    private TextView evtDetailDescription;

    /*
     * TextView object used to set the event date in the activity
     */
    private TextView evtDetailDate;

    /*
     * TextView object used to set the event address in the activity
     */
    private TextView evtDetailAddress;

    /*
     * ImageView object used to set the event image in the activity
     */
    private ImageView evtDetailImage;

    /*
     * ShareActionProvider object used to provide share option in  the action bar
     */
    private ShareActionProvider mShareActionProvider;

    /**
     * ActionBar attribute to manage the top action bar items
     */
    private ActionBar actionBar;

    /**
     * This method overrides the onCreate method of Activity class Initialize
     * activity components
     *
     * @param savedInstanceState
     */
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_event_detail);
        initElements();

    }

    /**
     * This method overrides the onCreateOptionsMenu method of Activity class
     * Initialize menu components
     *
     * @param menu Menu class instance
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.evt_detail_menu, menu);

        // Set up ShareActionProvider's default share intent
        MenuItem shareItem = menu.findItem(R.id.action_share);
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(getDefaultIntent());

        return super.onCreateOptionsMenu(menu);

    }


    /**
     * Defines a default (dummy) share intent to initialize the action provider.
     * However, as soon as the actual content to be used in the intent
     * is known or changes, you must update the share intent by again calling
     * mShareActionProvider.setShareIntent()
     */
    private Intent getDefaultIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        return intent;
    }

    /**
     * This method links the activity components with the class components
     * (EditText, Button , etc)
     */
    public void initElements() {

        Intent intent = getIntent();
        eventObj = (EventUI) intent.getSerializableExtra("eventObj");

        evtDetailDescription = (TextView) findViewById(R.id.evtDetailDescription);
        evtDetailDescription.setText(eventObj.getEventDescription());

        evtDetailDate = (TextView) findViewById(R.id.evtDetailDate);
        evtDetailDate.setText(eventObj.getEventDateView());

        evtDetailAddress = (TextView) findViewById(R.id.evtDetailAddress);
        evtDetailAddress.setText(eventObj.getEventAddress()+" "+getResources().getString(R.string.howToArriveStr));

        evtDetailImage = (ImageView) findViewById(R.id.evtDetailImg);

        // ImageLoader class instance
        ImageLoader imgLoader = new ImageLoader(getApplicationContext());
        int loader = R.drawable.ic_loading;
        //imgLoader.DisplayImage(eventObj.getEventImgView(), loader, evtDetailImage, 300);

        // setting up the action bar
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    /**
     * This method listen to onClick events and execute some sentences depending
     * on the clicked component
     *
     * @param v View object
     */
    public void ButtonOnClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.evtDetailStreetView:
                intent = new Intent(EventDetailActivity.this, EventMapPanoramaFragment.class);
                startActivity(intent);

                break;

            case R.id.evtDetailRoute:
                intent = new Intent(EventDetailActivity.this, EventRouteActivity.class);
                startActivity(intent);

                break;

        }
    }
}
