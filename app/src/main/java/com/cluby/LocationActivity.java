package com.cluby;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.location.Geocoder;
import android.location.Address;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.cluby.utils.FontsOverride;
import com.cluby.utils.LoadingAsyncTask;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * This class uses the google play services tools to get the current location of the user.
 *
 * @author W&L
 * @version 1.0
 */
public class LocationActivity extends Activity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    /*
     * Stores the current instantiation of the location client in this object
     */
    private LocationClient mLocationClient;

    /*
     * Global variable to hold the current location
     */
    private Location mCurrentLocation;

    /*
     * TextView to set the city and the country name in case to location works
     */
    private TextView currentCityCountryTxt;

    /*
     * boolean attribute to check if location and gps tool is enabled
     */
    private boolean locationEnabled;


    /*
     * TextView to use when gps is disabled
     */
    private TextView verifyGpsTxt;

    /*
     * manager location object
     */
    private LocationManager manager;

    /*
     *  dialog progress to use when location settings is selected
     */
    private ProgressDialog gpsProgressDialog;

    /**
     * This method overrides the onCreate method of Activity class Initialize
     * activity components
     *
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initElements();

    }


    /**
     * This method listen to onClick events and execute some sentences depending
     * on the clicked component
     */
    public void ButtonOnClick(View v) {
        Intent intent = null;
        LoadingAsyncTask loadObj;

        switch (v.getId()) {
            case R.id.acceptBtn:
                intent = new Intent(LocationActivity.this, WelcomeActivity.class);
                loadObj = new LoadingAsyncTask(LocationActivity.this, intent);
                loadObj.execute();
                break;

            case R.id.skipBtn:
                intent = new Intent(LocationActivity.this, WelcomeActivity.class);
                loadObj = new LoadingAsyncTask(LocationActivity.this, intent);
                loadObj.execute();
                break;

            case R.id.verifyGpsTxt:
                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                break;
        }
    }


    /*
     * Handle results returned to the FragmentActivity
     * by Google Play services
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST:

                switch (resultCode) {

                    case 0:

                        mLocationClient = new LocationClient(this, this, this);
                        manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

                        gpsProgressDialog = ProgressDialog.show(LocationActivity.this, "",
                                getResources().getString(R.string.loadingLocationStr), true);

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                verifyGpsTxt.setVisibility(View.GONE);

                                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                                    locationEnabled = false;
                                } else locationEnabled = true;

                                mLocationClient.connect();
                                gpsProgressDialog.hide();
                            }
                        }, 5000);

                        break;

                    case Activity.RESULT_OK:
                        break;
                }

        }
    }

    /*
     * Check that Google Play services is available
     */
    private boolean servicesConnected() {

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {

        }
        return true;
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle dataBundle) {

        if (servicesConnected() && mLocationClient.isConnected() && locationEnabled) {

            String cityName = "";
            String countryName = "";
            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());

            try {

                mCurrentLocation = mLocationClient.getLastLocation();
                List<Address> addresses = gcd.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 1);

                if (addresses.size() > 0) {
                    cityName = addresses.get(0).getLocality();
                    countryName = addresses.get(0).getCountryName();

                    String res = "";
                    if (!cityName.equals("")) {
                        res = res + cityName;
                    }

                    if (!countryName.equals("")) {
                        if (res.equals(""))
                            res = res + countryName;
                        else
                            res = res + ", " + countryName;
                    }

                    currentCityCountryTxt.setText(res);
                }
            } catch (IOException e) {
                verifyGpsTxt.setVisibility(View.VISIBLE);
                Log.e("LocationActivity", "IO Exception in getFromLocation()");

            }

            Log.d("com.cluby.LocationActivity.gpsConnectedSuccess", currentCityCountryTxt.getText().toString());
        } else {
            verifyGpsTxt.setVisibility(View.VISIBLE);
            Log.d("com.cluby.LocationActivity.gpsConnectedFailed", "GPS Inactive");
            currentCityCountryTxt.setText(getResources().getString(R.string.locationDisabledStr));
        }

    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
        // Display the connection status
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            //showErrorDialog(connectionResult.getErrorCode());
        }
    }

    /*
    * Called after onCreate(Bundle) — or after onRestart() when the activity had been stopped,
    * but is now again being displayed to the user. It will be followed by onResume().
    */
    @Override
    protected void onStart() {
        super.onStart();

    }

    /*
    * Called when the Activity is no longer visible.
    */
    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();
        super.onStop();
    }

    /**
     * This method links the activity components with the class components
     * (EditText, Button , etc)
     */
    public void initElements() {

        setContentView(R.layout.location_activity);

        mLocationClient = new LocationClient(this, this, this);

        LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationEnabled = false;
        } else locationEnabled = true;

        // Connect the client.
        mLocationClient.connect();

        currentCityCountryTxt = (TextView) findViewById(R.id.currentCityCountryTxt);
        verifyGpsTxt = (TextView) findViewById(R.id.verifyGpsTxt);

        final Typeface mFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf");
        final ViewGroup mContainer = (ViewGroup) findViewById(android.R.id.content).getRootView();
        FontsOverride.setAppFont(mContainer, mFont, true);
    }

}