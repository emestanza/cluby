package com.cluby;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.cluby.adapters.EventPostAdapter;
import com.cluby.model.EventPostUI;
import com.cluby.utils.AppConfig;
import com.cluby.utils.FontsOverride;
import com.cluby.utils.HttpAsyncTask;
import com.cluby.utils.HttpClubyResponse;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * This class is the first activity class for the app, which shows the
 * presentation of the app
 *
 * @author W&L
 * @version 1.0
 */
public class HappenNowFragment extends Fragment {

    private EventPostAdapter adapter;


    /**
     * ListView attribute to manage the event list
     */
    private ListView eventList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_happen_now,
                container, false);
        return view;
    }


    /*
    * Called after onCreate(Bundle) — or after onRestart() when the activity had been stopped,
    * but is now again being displayed to the user. It will be followed by onResume().
    */
    @Override
    public void onStart() {
        super.onStart();
        initElements();

        HttpAsyncTask reqClient = new HttpAsyncTask(getActivity().getApplicationContext(), "get");
        List parameters = AppConfig.getMainParameters();
        parameters.add(new BasicNameValuePair("url", AppConfig.GET_POST_EVENT_URL + "?event_id=1"));

        HttpClubyResponse result = null;
        try {
            result = (HttpClubyResponse) reqClient.execute(parameters).get();
            JSONArray posts = new JSONArray(result.getResponseBody());
            EventPostUI[] evList = new EventPostUI[posts.length()];

            for (int i = 0; i < posts.length(); i++) {

                EventPostUI eventObj = new EventPostUI();
                JSONObject evtJson = posts.getJSONObject(i);
                eventObj.setPostDescription(evtJson.getString("description"));
                eventObj.setEventId(1);
                eventObj.setPostMultimediaUrl(evtJson.getString("multimedia"));
                eventObj.setPostPubDate(evtJson.getString("pub_date"));
                eventObj.setPostSocNetwork(evtJson.getString("social_network"));
                eventObj.setPostUser(evtJson.getString("user"));
                evList[i] = eventObj;
            }

            adapter = new EventPostAdapter(getActivity(), evList);
            eventList.setAdapter(adapter);

        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * This method links the activity components with the class components
     * (EditText, Button , etc)
     */
    public void initElements() {

        eventList = (ListView) getView().findViewById(R.id.postEvtListView);
        final Typeface mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        final ViewGroup mContainer = (ViewGroup) getView().findViewById(R.id.evtHappenNowLayout).getRootView();
        FontsOverride.setAppFont(mContainer, mFont, true);

    }

}
