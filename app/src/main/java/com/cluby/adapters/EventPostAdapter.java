package com.cluby.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.cluby.R;
import com.cluby.model.EventPostUI;
import com.cluby.model.EventUI;
import com.cluby.utils.ImageLoader;

/**
 * CustomAdapter is a class to implement the adapter of the main event list view
 *
 * @author W&L
 * @version 1.0
 */
public class EventPostAdapter extends ArrayAdapter<EventPostUI> {
    private final Activity context;
    private final EventPostUI[] values;
    private ImageLoader imgLoader;

    public EventPostAdapter(Activity context, EventPostUI[] values) {
        super(context, R.layout.happen_now_row_layout, values);
        this.context = context;
        this.values = values;

    }

    public ImageLoader getImgLoader() {
        return imgLoader;
    }

    /*
    *  Static class to help to process the event data with the adapter object
    */
    static class ViewHolder {
        public ImageView postImg;
        public TextView  postDescription;
        public ImageView postUserImg;
        public ImageView postSocialNetImg;
        public TextView postUserTxt;
        public TextView postPubDateTxt;

    }


    /**
     * The ListView instance calls the getView() method on the adapter for each
     * data element. In this method the adapter creates the row layout and maps
     * the data to the views in the layout.
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder viewHolder = new ViewHolder();
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.happen_now_row_layout, null);

            viewHolder.postDescription = (TextView) rowView.findViewById(R.id.postDescription);
            viewHolder.postImg = (ImageView) rowView.findViewById(R.id.postImg);
            viewHolder.postSocialNetImg = (ImageView) rowView.findViewById(R.id.postSocialNetImg);
            viewHolder.postPubDateTxt = (TextView) rowView.findViewById(R.id.postPubDateTxt);
            viewHolder.postUserImg = (ImageView) rowView.findViewById(R.id.postUserImg);
            viewHolder.postUserTxt = (TextView) rowView.findViewById(R.id.postUserTxt);


            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        final Typeface mFont = Typeface.createFromAsset(context.getAssets(),"fonts/Raleway-Medium.ttf");
        viewHolder.postDescription.setTypeface(mFont);
        viewHolder.postPubDateTxt.setTypeface(mFont);
        viewHolder.postUserTxt.setTypeface(mFont);

        EventPostUI postObj = values[position];

        // ImageLoader class instance
        imgLoader = new ImageLoader(this.context);
        int loader = R.drawable.ic_loading;
        getImgLoader().DisplayImage(postObj.getPostMultimediaUrl(), loader, viewHolder.postImg, 300);

        viewHolder.postDescription.setText(postObj.getPostDescription());
        viewHolder.postUserTxt.setText(postObj.getPostUser());
        viewHolder.postUserTxt.setText(postObj.getPostUser());
        viewHolder.postPubDateTxt.setText(postObj.getPostPubDate());

        int socialResource = R.drawable.ic_launcher;

        if (postObj.getPostSocNetwork().equals("facebook")){
            socialResource = R.drawable.facebook_icon;
        }

        if (postObj.getPostSocNetwork().equals("twitter")){
            socialResource = R.drawable.twitter_icon;
        }

        if (postObj.getPostSocNetwork().equals("instagram")){
            socialResource = R.drawable.instagram_icon;
        }

        viewHolder.postSocialNetImg.setImageResource(socialResource);

        return rowView;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int position) {
        return false;
    }

}