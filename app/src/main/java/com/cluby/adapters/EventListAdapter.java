package com.cluby.adapters;

import android.graphics.Typeface;
import com.cluby.utils.ImageLoader;
import com.cluby.R;
import com.cluby.model.EventUI;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * CustomAdapter is a class to implement the adapter of the main event list view
 *
 * @author W&L
 * @version 1.0
 */
public class EventListAdapter extends ArrayAdapter<EventUI> {
    private final Activity context;
    private final EventUI[] values;
    private ImageLoader imgLoader;

    public EventListAdapter(Activity context, EventUI[] values) {
        super(context, R.layout.rowlayout, values);
        this.context = context;
        this.values = values;

    }

    public ImageLoader getImgLoader() {
        return imgLoader;
    }

    /*
    *  Static class to help to process the event data with the adapter object
    */
    static class ViewHolder {
        public TextView eventDate;
        public TextView eventName;
        public ImageView eventImg;
    }


    /**
     * The ListView instance calls the getView() method on the adapter for each
     * data element. In this method the adapter creates the row layout and maps
     * the data to the views in the layout.
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder viewHolder = new ViewHolder();
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.rowlayout, null);

            viewHolder.eventDate = (TextView) rowView.findViewById(R.id.eventListDateStr);
            viewHolder.eventName = (TextView) rowView.findViewById(R.id.eventListNameStr);
            viewHolder.eventImg = (ImageView) rowView.findViewById(R.id.eventListIc);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        final Typeface mFont = Typeface.createFromAsset(context.getAssets(),"fonts/Raleway-Medium.ttf");
        viewHolder.eventName.setTypeface(mFont);
        viewHolder.eventDate.setTypeface(mFont);

        EventUI s = values[position];
        viewHolder.eventName.setText(s.getEventNameView());
        viewHolder.eventDate.setText(s.getEventDateView());

        // ImageLoader class instance
        imgLoader = new ImageLoader(this.context);
        int loader = R.drawable.ic_loading;
        getImgLoader().DisplayImage(s.getEventImgView(), loader, viewHolder.eventImg, 70);

        return rowView;
    }

}