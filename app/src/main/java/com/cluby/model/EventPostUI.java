package com.cluby.model;

import java.io.Serializable;

/**
 * This class works as a Bean which is used to define the elements to create a row for the event list view
 *
 * @author Enmanuel Mestanza
 * @version 1.0
 */
public class EventPostUI implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int eventId;
    private String postPubDate;
    private String postMultimediaUrl;
    private String postDescription;
    private String postUser;
    private String postSocNetwork;

    public EventPostUI(){
        this.postPubDate = "";
        this.postMultimediaUrl = "";
        this.postDescription = "";
        this.postUser = "";
        this.postSocNetwork = "";
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getPostPubDate() {
        return postPubDate;
    }

    public void setPostPubDate(String postPubDate) {
        this.postPubDate = postPubDate;
    }

    public String getPostMultimediaUrl() {
        return postMultimediaUrl;
    }

    public void setPostMultimediaUrl(String postMultimediaUrl) {
        this.postMultimediaUrl = postMultimediaUrl;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public String getPostUser() {
        return postUser;
    }

    public void setPostUser(String postUser) {
        this.postUser = postUser;
    }

    public String getPostSocNetwork() {
        return postSocNetwork;
    }

    public void setPostSocNetwork(String postSocNetwork) {
        this.postSocNetwork = postSocNetwork;
    }
}
