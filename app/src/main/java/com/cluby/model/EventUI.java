package com.cluby.model;

import java.io.Serializable;

/**
 * This class works as a Bean which is used to define the elements to create a row for the event list view
 *
 * @author Enmanuel Mestanza
 * @version 1.0
 */
public class EventUI implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public int eventId;
    public String eventDateView;
    public String eventNameView;
    public String eventImgView;
    public String eventAddress;
    public String eventDescription;
    private Double eventLatitude;
    private Double eventLongitude;

    public EventUI() {
        this.eventId = 0;
        this.eventDateView = "";
        this.eventNameView = "";
        this.eventImgView = "";
        this.eventAddress = "";
        this.eventDescription = "";
        this.setEventLatitude(-1.0);
        this.setEventLongitude(-1.0);
    }

    /**
     * @return the eventId
     */
    public int getEventId() {
        return eventId;
    }

    /**
     * @param eventId the eventId to set
     */
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    /**
     * @return the eventDateView
     */
    public String getEventDateView() {
        return eventDateView;
    }

    /**
     * @param eventDateView the eventDateView to set
     */
    public void setEventDateView(String eventDateView) {
        this.eventDateView = eventDateView;
    }

    /**
     * @return the eventNameView
     */
    public String getEventNameView() {
        return eventNameView;
    }

    /**
     * @param eventNameView the eventNameView to set
     */
    public void setEventNameView(String eventNameView) {
        this.eventNameView = eventNameView;
    }

    /**
     * @return the eventImgView
     */
    public String getEventImgView() {
        return eventImgView;
    }

    /**
     * @param eventImgView the eventImgView to set
     */
    public void setEventImgView(String eventImgView) {
        this.eventImgView = eventImgView;
    }

    /**
     * @return the eventAddress
     */
    public String getEventAddress() {
        return eventAddress;
    }

    /**
     * @param eventAddress the eventAddress to set
     */
    public void setEventAddress(String eventAddress) {
        this.eventAddress = eventAddress;
    }

    /**
     * @return the eventDescription
     */
    public String getEventDescription() {
        return eventDescription;
    }

    /**
     * @param eventDescription the eventDescription to set
     */
    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public Double getEventLatitude() {
        return eventLatitude;
    }

    public void setEventLatitude(Double eventLatitude) {
        this.eventLatitude = eventLatitude;
    }

    public Double getEventLongitude() {
        return eventLongitude;
    }

    public void setEventLongitude(Double eventLongitude) {
        this.eventLongitude = eventLongitude;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "[eventId=" + eventId + ", eventDateView="
                + eventDateView + ", eventNameView=" + eventNameView
                + ", eventImgView=" + eventImgView + ", eventAddress="
                + eventAddress + ", eventDescription=" + eventDescription + "]";
    }

}
