package com.cluby.model;

import com.cluby.utils.DBConnect;

public class User {

    private String email;
    private String userName;
    private String name;
    private String password;

    public User() {

    }

    public User(String email, String password) {
        super();
        this.email = email;
        this.password = password;
        this.name = "Usuario";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void register(User userObj) {
        DBConnect dbObj = DBConnect.getInstance();
        dbObj.users.add(userObj);
    }

    public boolean login() {
        DBConnect dbObj = DBConnect.getInstance();

        return dbObj.users.contains(this);

    }

    public void get(int idUser) {

    }

    public void edit(int idUser) {

    }

    public void delete(int idUser) {

    }

}
