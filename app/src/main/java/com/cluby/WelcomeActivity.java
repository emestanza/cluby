package com.cluby;

import java.util.List;
import java.util.concurrent.ExecutionException;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.view.ViewGroup;
import com.cluby.adapters.EventListAdapter;
import com.cluby.utils.HttpAsyncTask;
import com.cluby.utils.AppConfig;
import com.cluby.utils.HttpClubyResponse;
import com.cluby.utils.FontsOverride;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.cluby.model.EventUI;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * This class is attached to the welcome activity, so it covers all the logic
 * related to timeline and main screen after login
 *
 * @author W&L
 * @version 1.0
 */
public class WelcomeActivity extends ActionBarActivity {

    /**
     * ActionBar attribute to manage the top action bar items
     */
    private ActionBar actionBar;

    /**
     * ListView attribute to manage the event list
     */
    private ListView eventList;

    /**
     * Adapter to be applied to event list
     */
    private EventListAdapter adapter;

    /**
     * This method overrides the onCreate method of Activity class Initialize
     * activity components
     *
     * @param savedInstanceState
     */
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        initElements();
    }

    /**
     * This method overrides the onCreateOptionsMenu method of Activity class
     * Initialize menu components
     *
     * @param menu Menu class instance
     * @return boolean
     */
    @SuppressLint("NewApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.init, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem obj = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(obj);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true); // Iconify the widget

        return true;
    }

    /**
     * This method links the activity components with the class components
     * (EditText, Button , etc)
     */
    @SuppressLint("NewApi")
    public void initElements() {
        eventList = (ListView) findViewById(R.id.listview);
        // setting up the action bar
        actionBar = getActionBar();
        actionBar.setTitle(getResources().getString(R.string.nextEventStr));

        String query = "";
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
            // At the end of doMySearch(), you can populate
            // a String Array as resultStringArray[] and set the Adapter

        }

        HttpAsyncTask reqClient = new HttpAsyncTask(this, "get");
        List parameters = AppConfig.getMainParameters();
        //parameters.add(new BasicNameValuePair("url", AppConfig.GET_EVENTS_URL + "?query_str=" + query));
        parameters.add(new BasicNameValuePair("url", AppConfig.GET_EVENTS_URL));

        HttpClubyResponse result = null;

        try {
            result = (HttpClubyResponse) reqClient.execute(parameters).get();
            JSONArray eventos = new JSONArray(result.getResponseBody());
            EventUI[] evList = new EventUI[eventos.length()];


            for (int i = 0; i < eventos.length(); i++) {
                EventUI eventObj = new EventUI();
                JSONObject evtJson = eventos.getJSONObject(i);
                eventObj.setEventId(Integer.parseInt(evtJson.getString("pk")));
                JSONObject evtFields = evtJson.getJSONObject("fields");

                eventObj.setEventNameView(evtFields.getString("name"));
                eventObj.setEventDateView(evtFields.getString("beginning"));
                eventObj.setEventAddress("Lorem ipsum dolor kfjdslkfjskdlf jlkfj dsklf jsdklfjdsl k");
                eventObj.setEventDescription(evtFields.getString("description"));
                eventObj.setEventLatitude(-12.037992);
                eventObj.setEventLongitude(-77.065484);
                eventObj.setEventImgView(AppConfig.BASE_API_URL+"/media/"+evtFields.getString("image"));
/*
                if (evtJson.getDouble("latitude") != -1.0 &&  evtJson.getDouble("longitude") != -1.0){
                    eventObj.setEventLatitude(evtJson.getDouble("latitude"));
                    eventObj.setEventLongitude(evtJson.getDouble("longitude"));
                }*/

                evList[i] = eventObj;
            }

            adapter = new EventListAdapter(this, evList);
            eventList.setAdapter(adapter);
            eventList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

                    EventUI eventObj = (EventUI) eventList.getItemAtPosition(position);
                    //Intent intentObj = new Intent(WelcomeActivity.this, EventDetailActivity.class);
                    Intent intentObj = new Intent(WelcomeActivity.this, EventDetailPager.class);
                    intentObj.putExtra("eventObj", eventObj);
                    adapter.getImgLoader().clearCache();

                    startActivity(intentObj);

                }

            });

        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        final Typeface mFont = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf");
        final ViewGroup mContainer = (ViewGroup) findViewById(android.R.id.content).getRootView();
        FontsOverride.setAppFont(mContainer, mFont, true);
    }

    /*
    * Called after onCreate(Bundle) — or after onRestart() when the activity had been stopped,
    * but is now again being displayed to the user. It will be followed by onResume().
    */
    @Override
    protected void onStart() {
        super.onStart();

        //Utils.sendAlertMessage(this, getResources().getString(R.string.clubyWelcomeHeaderStr), getResources().getString(R.string.clubyWelcomeContentStr));
    }

    /*
    * Method called when item menu is selected
    */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.addEventBtn:
                intent = new Intent(WelcomeActivity.this, NewEventActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}