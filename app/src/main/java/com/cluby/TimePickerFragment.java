package com.cluby;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.widget.EditText;
import android.widget.TimePicker;
import java.util.Calendar;

/**
 * This class is used to create a timepicker dialog
 * Created by W&L on 07/05/2014.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    /**
     * Get the Activity object attached to the fragment
     */
    private FragmentActivity activityRelated;

    /**
     * Get the EditText to set the time selected by the user
     */
    private EditText timeFieldTxt;

    /**
     * String to set AM or PM in the result
     */
    private String amPm;

    /**
     * Process the hour and minutes of the time selected by the user
     */
    private Calendar datetime;

    /**
     * It helps to set a minute like '00' format
     */
    private String auxMin;

    /**
     * It helps to set an hour in 12hours format
     */
    private String strHrsToShow;

    /**
     * It gets the current time when user pops up the dialog
     */
    private Calendar currentTime;

    private int dateIdTxt;


    public TimePickerFragment(int dateId){
        dateIdTxt = dateId;
    }

    /**
     * Override to build your own custom Dialog container. This is typically used to show an AlertDialog instead
     * of a generic Dialog; when doing so, onCreateView(LayoutInflater, ViewGroup, Bundle) does not need to be
     * implemented since the AlertDialog takes care of its own content.
     *
     * @param savedInstanceState
     * @return DatePickerDialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    /**
     * Process the result after dialog is closed
     *
     * @param view
     * @param hourOfDay
     * @param minute
     */
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        activityRelated = getActivity();
        timeFieldTxt = (EditText)activityRelated.findViewById(dateIdTxt);
        amPm = "";
        datetime = Calendar.getInstance();
        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        datetime.set(Calendar.MINUTE, minute);

        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
            amPm = "AM";
        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
            amPm = "PM";

        auxMin = Integer.toString(datetime.get(Calendar.MINUTE));
        if (datetime.get(Calendar.MINUTE) < 10) auxMin = "0"+datetime.get(Calendar.MINUTE);

        strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ?"12":datetime.get(Calendar.HOUR)+"";
        if (strHrsToShow.length()==1)strHrsToShow = "0"+strHrsToShow;

        timeFieldTxt.setText(strHrsToShow+":"+auxMin+" "+amPm);
    }
}
