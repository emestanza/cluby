package com.cluby;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.cluby.model.EventUI;
import com.cluby.utils.FontsOverride;
import com.cluby.utils.ImageLoader;

/**
 * This class is the first activity class for the app, which shows the
 * presentation of the app
 *
 * @author Enmanuel Mestanza
 * @version 1.0
 */
public class EventDetailFragment extends Fragment {

    /*
     * EventUI object to be used to process the JSON array taken by API
     */
    private EventUI eventObj;

    /*
     * TextView object used to set the event name in the activity
     */
    private TextView evtDetailName;

    /*
     * TextView object used to set the event description in the activity
     */
    private TextView evtDetailDescription;

    /*
     * TextView object used to set the event date in the activity
     */
    private TextView evtDetailDate;

    /*
     * TextView object used to set the event address in the activity
     */
    private TextView evtDetailAddress;

    /*
     * TextView object used to set the event address in the activity
     */
    private TextView evtDetailMap;

    /*
     * ImageView object used to set the event image in the activity
     */
    private ImageView evtDetailImage;


    private Double evtDetailLatitude;

    private Double evtDetailLongitude;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_event_detail,
                container, false);
        return view;
    }


    /*
    * Called after onCreate(Bundle) — or after onRestart() when the activity had been stopped,
    * but is now again being displayed to the user. It will be followed by onResume().
    */
    @Override
    public void onStart() {
        super.onStart();

        initElements();
    }


    /**
     * This method links the activity components with the class components
     * (EditText, Button , etc)
     */
    public void initElements() {

        //Intent intent = getIntent();
        Bundle args = getArguments();
        eventObj = (EventUI) args.getSerializable("eventObjFr");

        evtDetailName = (TextView) getView().findViewById(R.id.evtDetailName);
        evtDetailName.setText(eventObj.getEventNameView());

        evtDetailDescription = (TextView) getView().findViewById(R.id.evtDetailDescription);
        evtDetailDescription.setText(eventObj.getEventDescription());

        evtDetailDate = (TextView) getView().findViewById(R.id.evtDetailDate);
        evtDetailDate.setText(eventObj.getEventDateView());

        evtDetailAddress = (TextView) getView().findViewById(R.id.evtDetailAddress);
        evtDetailAddress.setText(getResources().getString(R.string.howToArriveStr));

        evtDetailMap = (TextView) getView().findViewById(R.id.evtDetailMap);
        evtDetailMap.setText(eventObj.getEventAddress()+". "+getResources().getString(R.string.seeMapPanoramicStr));

        evtDetailLatitude = eventObj.getEventLatitude();
        evtDetailLongitude = eventObj.getEventLongitude();

        evtDetailImage = (ImageView) getView().findViewById(R.id.evtDetailImg);

        // ImageLoader class instance
        ImageLoader imgLoader = new ImageLoader(getActivity().getApplicationContext());
        int loader = R.drawable.ic_loading;

        ProgressBar pbar = (ProgressBar)getView().findViewById(R.id.loadingPanelEvtImg);

        //evtDetailImage.setImageResource(android.R.color.transparent);
        imgLoader.DisplayImage(eventObj.getEventImgView(), loader, evtDetailImage, 300, pbar);

        final Typeface mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Medium.ttf");
        final ViewGroup mContainer = (ViewGroup) getView().findViewById(R.id.evtDetailLayout).getRootView();
        FontsOverride.setAppFont(mContainer, mFont, true);

    }

}
