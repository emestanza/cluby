package com.cluby;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

/**
 * This class is the first activity class for the app, which shows the
 * presentation of the app
 *
 * @author W&L
 * @version 1.0
 */
public class InitActivity extends Activity {

    /**
     * This method overrides the onCreate method of Activity class Initialize
     * activity components
     *
     * @param savedInstanceState saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This sentences makes the action bar dissapear in this activity
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_init);
        showLauncher();
    }

    /**
     * This method overrides the onCreateOptionsMenu method of Activity class
     * Initialize menu components
     *
     * @param menu Menu class instance
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    /**
     * This method generates a thread to sleep the first activity for some
     * seconds, and after redirect to the logic
     *
     */
    private void showLauncher() {
        Thread launcherThread = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    Intent intentObj = new Intent(InitActivity.this,
                            LoginActivity.class);
                    startActivity(intentObj);
                }
            }
        };

        launcherThread.start();
    }

}