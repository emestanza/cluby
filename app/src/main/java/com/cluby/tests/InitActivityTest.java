package com.cluby.tests;

import android.annotation.SuppressLint;
import android.test.ActivityInstrumentationTestCase2;
import com.cluby.LocationActivity;
import com.cluby.LoginActivity;
import com.cluby.WelcomeActivity;
import com.robotium.solo.Solo;
import junit.framework.Assert;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.cluby.InitActivityTest \
 * com.cluby.tests/android.test.InstrumentationTestRunner
 */
@SuppressLint("NewApi")
public class InitActivityTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    public InitActivityTest() {
        super(LoginActivity.class);
    }

    private Solo solo;

    @Override
    public void setUp() throws Exception {
        //setUp() is run before a test case is started.
        //This is where the solo object is created.
        solo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    public void tearDown() throws Exception {
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo.finishOpenedActivities();
    }

    public void testLogin() throws Exception {

        solo.clickOnButton("Entrar");
        Assert.assertTrue(solo.waitForLogMessage("com.cluby.LoginActivity.invalidForm"));

        solo.clearLog();
        solo.enterText(0, "admin");
        solo.clickOnButton("Entrar");
        Assert.assertTrue(solo.waitForLogMessage("com.cluby.LoginActivity.invalidForm"));
        solo.clearLog();

        solo.enterText(1, "malo");
        solo.clickOnButton("Entrar");
        Assert.assertTrue(solo.waitForLogMessage("com.cluby.LoginActivity.loginError"));
        solo.clearLog();

        solo.enterText(1, "");
        solo.enterText(1, "cluby2050");
        solo.clickOnButton("Entrar");
        Assert.assertTrue(solo.waitForLogMessage("com.cluby.LoginActivity.loginSuccess", 5000));

        solo.clearLog();
        solo.waitForActivity(LocationActivity.class);
        solo.assertCurrentActivity("wrong activity", LocationActivity.class);
        solo.waitForLogMessage("Location Updates");

        if (!solo.waitForLogMessage("com.cluby.LocationActivity.gpsConnectedSuccess")){
            solo.waitForLogMessage("com.cluby.LocationActivity.gpsConnectedFailed");
            solo.clickOnButton("OMITIR");
        }
        else{
            solo.clickOnButton("ACEPTAR");
        }

        solo.clearLog();
        solo.waitForActivity(WelcomeActivity.class);
        solo.assertCurrentActivity("wrong activity", WelcomeActivity.class);

        solo.clickOnButton("OK");
        solo.clickInList(1);
        assertTrue(solo.waitForText("Metallica"));

    }

}
