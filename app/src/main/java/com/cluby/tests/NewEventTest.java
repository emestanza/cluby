package com.cluby.tests;

import android.annotation.SuppressLint;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;
import com.cluby.*;
import com.robotium.solo.Solo;
import junit.framework.Assert;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.cluby.InitActivityTest \
 * com.cluby.tests/android.test.InstrumentationTestRunner
 */
@SuppressLint("NewApi")
public class NewEventTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    public NewEventTest() {
        super(LoginActivity.class);
    }

    private Solo solo;

    @Override
    public void setUp() throws Exception {
        //setUp() is run before a test case is started.
        //This is where the solo object is created.
        solo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    public void tearDown() throws Exception {
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo.finishOpenedActivities();
    }

    public void testNewEvent() throws Exception {

        solo.clearLog();
        solo.enterText(0, "admin");
        solo.enterText(1, "cluby2050");
        solo.clickOnButton("Entrar");
        solo.clearLog();
        solo.waitForActivity(LocationActivity.class);
        solo.assertCurrentActivity("wrong activity", LocationActivity.class);
        solo.waitForLogMessage("Location Updates");

        solo.clickOnButton("ACEPTAR");

        solo.clearLog();
        solo.waitForActivity(WelcomeActivity.class);
        solo.assertCurrentActivity("wrong activity", WelcomeActivity.class);

        solo.clickOnButton("OK");
        solo.clickOnActionBarItem(R.id.addEventBtn);

        solo.assertCurrentActivity("wrong activity", NewEventActivity.class);
        solo.clickOnActionBarItem(R.id.createEventBtn);
        solo.enterText(0, "Evento 1");
        solo.clickOnActionBarItem(R.id.createEventBtn);

        EditText aux = (EditText)solo.getView(R.id.dateTxt);
        solo.clickOnView(aux);
        solo.clickOnButton("Set");
        solo.clickOnActionBarItem(R.id.createEventBtn);

        aux = (EditText)solo.getView(R.id.timeTxt);
        solo.clickOnView(aux);
        solo.clickOnButton("Set");
        solo.clickOnActionBarItem(R.id.createEventBtn);

        aux = (EditText)solo.getView(R.id.dateEndTxt);
        solo.clickOnView(aux);
        solo.clickOnButton("Set");
        solo.clickOnActionBarItem(R.id.createEventBtn);

        aux = (EditText)solo.getView(R.id.timeEndTxt);
        solo.clickOnView(aux);
        solo.clickOnButton("Set");
        solo.clickOnActionBarItem(R.id.createEventBtn);

        solo.enterText(5, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        solo.enterText(6, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");

        solo.clickOnButton("Obtener Mapa del Evento");
        Assert.assertTrue(solo.waitForLogMessage("com.cluby.NewEventTest.getMap", 5000));
        solo.clickOnActionBarItem(R.id.addMapPositionBtn);

        solo.clickOnActionBarItem(R.id.createEventBtn);
    }

}
