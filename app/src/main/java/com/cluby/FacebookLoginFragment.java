package com.cluby;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;

import java.util.Arrays;

/**
 * Created by W&L on 22/05/2014.
 */
public class FacebookLoginFragment extends Fragment {

    /**
     * String used to set a log information
     */
    private static final String TAG = "FacebookLoginFragment";

    /**
     * UiLifecycleHelper attribute to manage ui workflow with facebook login button
     */
    private UiLifecycleHelper uiHelper;

    /**
     * LoginButton attribute to manage facebook login button
     */
    private LoginButton authButton;


    /**
     * This method overrides the onCreate method of Activity class Initialize
     * activity components
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    /**
     * Called to have the fragment instantiate its user interface view. This is optional, and non-graphical
     * fragments can return null (which is the default implementation). This will be called between
     * onCreate(Bundle) and onActivityCreated(Bundle).
     *
     * @param inflater           The LayoutInflater object that can be used to inflate any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's UI should be attached to. The fragment should not add the view itself, but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     */
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fb_button, container, false);

        authButton = (LoginButton) view.findViewById(R.id.authButton);
        authButton.setFragment(this);
        authButton.setReadPermissions(Arrays.asList("public_profile", "user_friends", "email"));
        return view;
    }


    private void onSessionStateChange(Session session, SessionState state, Exception exception) {

        if (state.isOpened()) {
            Log.i(TAG, "Logged in...");
            Intent intent = null;
            intent = new Intent(getActivity(), LocationActivity.class);
            startActivity(intent);

        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
        }
    }

    /**
     * StatusCallback attribute to set a callback method to check if facebook login was successfully opened
     */
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }

    };

    /**
     * Called after onRestoreInstanceState(Bundle), onRestart(), or onPause(), for your activity to start
     * interacting with the user. This is a good place to begin animations, open exclusive-access devices
     * (such as the camera), etc.
     * Keep in mind that onResume is not the best indicator that your activity is visible to the user; a
     * system window such as the keyguard may be in front. Use onWindowFocusChanged(boolean) to know for
     * certain that your activity is visible to the user (for example, to resume a game).
     */
    @Override
    public void onResume() {
        super.onResume();

        // For scenarios where the main activity is launched and user
        // session is not null, the session state change notification
        // may not be triggered. Trigger it if it's open/closed.
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();

    }

    /**
     * Called when an activity you launched exits, giving you the requestCode you started it with, the
     * resultCode it returned, and any additional data from it. The resultCode will be RESULT_CANCELED if
     * the activity explicitly returned that, didn't return any result, or crashed during its operation.
     * You will receive this call immediately before onResume() when your activity is re-starting.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Called as part of the activity lifecycle when an activity is going into the background, but has not (yet)
     * been killed. The counterpart to onResume().
     * When activity B is launched in front of activity A, this callback will be invoked on A. B will not be
     * created until A's onPause() returns, so be sure to not do anything lengthy here.
     */
    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    /**
     * Perform any final cleanup before an activity is destroyed. This can happen either because the activity is
     * finishing (someone called finish() on it, or because the system is temporarily destroying this instance of
     * the activity to save space. You can distinguish between these two scenarios with the isFinishing() method.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    /**
     * Called to retrieve per-instance state from an activity before being killed so that the state can be
     * restored in onCreate(Bundle) or onRestoreInstanceState(Bundle) (the Bundle populated by this method will
     * be passed to both).
     *
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }
}