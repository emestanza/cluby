package com.cluby;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by W&L on 29/05/2014.
 */
@SuppressLint("NewApi")
public class EventMapPanoramaFragment extends ActionBarActivity {

    /**
     * Get the latitude and longitude of the event location
     */
    private LatLng eventLocation;

    /**
     * Get the Street View panorama of the eventLocation attribute
     */
    private StreetViewPanorama eventPanorama;

    /**
     * ActionBar attribute to manage the top action bar items
     */
    private ActionBar actionBar;

    /**
     * GoogleMap attribute to manage map into UI
     */
    private GoogleMap eventMap;

    /**
     * Fragment to set the map
     */
    private SupportMapFragment mapFragment;

    private Double evtLatitude;
    private Double evtLongitude;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initElements(savedInstanceState);
    }

    /**
     * Called after onCreate(Bundle) — or after onRestart() when the activity had been stopped,
     * but is now again being displayed to the user. It will be followed by onResume().
     */
    @Override
    protected void onStart() {
        super.onStart();

        // Move the camera instantly to hamburg with a zoom of 15.
        eventMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(eventLocation.latitude, eventLocation.longitude), 15));

        // Zoom in, animating the camera.
        eventMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);

        eventMap.addMarker(new MarkerOptions()
                .position(new LatLng(eventLocation.latitude, eventLocation.longitude))
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_cluby_pin)));

    }

    /**
     * Method to set the view of the panorama in streetview
     *
     * @param savedInstanceState
     */
    private void setUpStreetViewPanoramaIfNeeded(Bundle savedInstanceState) {
        if (eventPanorama == null) {

            eventPanorama = ((SupportStreetViewPanoramaFragment)
                    getSupportFragmentManager().findFragmentById(R.id.streetviewpanorama))
                    .getStreetViewPanorama();
            if (eventPanorama != null) {
                if (savedInstanceState == null) {
                    eventPanorama.setPosition(eventLocation);
                }
            }
        }
    }

    /**
     * This method links the activity components with the class components
     * (EditText, Button , etc)
     */
    public void initElements(Bundle savedInstanceState) {
        setContentView(R.layout.street_view_activity);

        Intent intent = getIntent();
        evtLatitude = intent.getDoubleExtra("latitude", -1.0);
        evtLongitude = intent.getDoubleExtra("longitude", -1.0);

        //replace the latitude and longitude values with the values of the event
        eventLocation = new LatLng(evtLatitude, evtLongitude);

        setUpStreetViewPanoramaIfNeeded(savedInstanceState);

        // Set up the action bar.
        actionBar = getActionBar();
        actionBar.setTitle(getResources().getString(R.string.locationPanoramicStr));
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Getting reference to the SupportMapFragment
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapEvtDetail);

        // Getting reference to the Google Map
        eventMap = mapFragment.getMap();

    }

}